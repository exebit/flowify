sudo podman run --rm --privileged multiarch/qemu-user-static --reset -p yes
sudo podman build -f Dockerfile -t flowify_server
sudo podman run --rm --network=host -v .:/src flowify_server
