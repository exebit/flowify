import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType
from .models import UserModel, CameraModel, MeasurementModel, db_session
import sys
from time import time

class User(SQLAlchemyObjectType):
    class Meta:
        model = UserModel
        # use `only_fields` to only expose specific fields ie "name"
        # only_fields = ("name",)
        # use `exclude_fields` to exclude specific fields ie "last_name"
        # exclude_fields = ("last_name",)

class Camera(SQLAlchemyObjectType):
  class Meta:
    model = CameraModel

class Measurement(SQLAlchemyObjectType):
  class Meta:
    model = MeasurementModel

class Query(graphene.ObjectType):
    users = graphene.List(User)
    cameras = graphene.List(Camera)
    measurements = graphene.List(Measurement)

    def resolve_users(self, info):
        query = User.get_query(info)  # SQLAlchemy query
        return query.all()
    
    def resolve_cameras(self, info):
        query = Camera.get_query(info)  # SQLAlchemy query
        return query.all()

    def resolve_measurements(self, info):
      query = Measurement.get_query(info)
      return query.all()

class CreateUser(graphene.Mutation):
    class Arguments:
        given_name = graphene.String()
        family_name = graphene.String()

    user = graphene.Field(lambda: User)

    def mutate(root, info, given_name, family_name):
        user = UserModel(given_name=given_name, family_name=family_name)
        db_session.add(user)
        db_session.flush()
        return CreateUser(user=user)

class CreateCamera(graphene.Mutation):
    class Arguments:
        pass

    camera = graphene.Field(lambda: Camera)

    def mutate(root, info):
        camera = CameraModel()
        db_session.add(camera)
        db_session.flush()
        return CreateCamera(camera=camera)

class CreateMeasurement(graphene.Mutation):
  class Arguments:
    camera_id = graphene.Int()
    people_count = graphene.Int()
  measurement = graphene.Field(lambda: Measurement)
  def mutate(root, info, camera_id, people_count):
    print(camera_id, file=sys.stderr)
    measurement = MeasurementModel(camera_id=camera_id, people_count=people_count, ts=int(time()))
    db_session.add(measurement)
    db_session.flush()
    return CreateMeasurement(measurement=measurement)
  

class Mutations(graphene.ObjectType):
    create_user = CreateUser.Field()
    create_camera = CreateCamera.Field()
    create_measurement = CreateMeasurement.Field()

schema = graphene.Schema(query=Query, mutation=Mutations)
