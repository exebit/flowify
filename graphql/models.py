from sqlalchemy import *
from sqlalchemy.orm import scoped_session, sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy.schema import CreateSchema

engine = create_engine('sqlite:///database.sqlite3', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=True,
                                         autoflush=True,
                                         bind=engine))

Base = declarative_base()
# We will need this for querying
Base.query = db_session.query_property()

class UserModel(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    given_name = Column(String)
    family_name = Column(String)

class CameraModel(Base):
    __tablename__ = 'camera'
    id = Column(Integer, primary_key=True)
    measurements = relationship("MeasurementModel", back_populates="camera")

class MeasurementModel(Base):
    __tablename__ = 'measurement'
    id = Column(Integer, primary_key=True)
    ts = Column(Integer)
    camera_id = Column(Integer, ForeignKey('camera.id'))
    camera = relationship("CameraModel", back_populates="measurements")
    people_count = Column(Integer)


Base.metadata.create_all(engine)
