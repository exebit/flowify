import 'package:crypted_preferences/crypted_preferences.dart';
import 'package:flutter/material.dart';
import 'package:web/CrowdMeasurement.dart';
import 'package:web/api.dart';
import 'package:web/charts.dart';
import 'package:web/images.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    Map swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }

    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });

    return MaterialColor(color.value, swatch);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flowify',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: createMaterialColor(Color.fromRGBO(20, 20, 20, 1)),
      ),
      home: MyHomePage(title: 'Flowify'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _textFieldController = TextEditingController();

  String api = "http://localhost:3002";
  List<CrowdMeasurement> data;
  Preferences prefs;

  @override
  void initState() {
    super.initState();
    _textFieldController.text = api;
    init();
  }

  Future<void> init() async {
    prefs = await Preferences.preferences(path: "/main");
    prefs.getString("url", defaultValue: "http://localhost:3002");
    var t = await getMeasurements(api);
    setState(() {
      data = t;
    });
    backgroundTask();
  }

  Future<void> backgroundTask() async {
    while (true) {
      print("Refresh");
      await Future.delayed(Duration(seconds: 5));
      var t = await getMeasurements(api);
      setState(() {
        data = t;
      });
    }
  }

  Future<void> _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Setting(s)'),
            content: TextField(
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "Enter new api ip"),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('cancel'),
                onPressed: () {
                  setState(() {
                    api = _textFieldController.value.text;
                  });
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  String date = "Day";

  Widget _dateDropDown() => Theme(
        data: Theme.of(context)
            .copyWith(canvasColor: Theme.of(context).primaryColor),
        child: DropdownButton<String>(
          style: TextStyle(color: Colors.blue),
          items: [
            DropdownMenuItem(
              value: "Day",
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(
                    Icons.view_day,
                    color: Colors.blue,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Day",
                  ),
                ],
              ),
            ),
            DropdownMenuItem(
              value: "Week",
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(
                    Icons.calendar_today,
                    color: Colors.blue,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Week",
                  ),
                ],
              ),
            ),
          ],
          onChanged: (value) {
            setState(() {
              date = value;
            });
          },
          value: date,
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Container(
          color: Color.fromRGBO(20, 20, 20, 1),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Material(
                  color: Color.fromRGBO(20, 20, 20, 1),
                  child: InkWell(
                    onTap: () {},
                    child: ListTile(
                      leading: Icon(
                        Icons.home,
                        size: 30,
                        color: Colors.blue,
                      ),
                      title: Text(
                        'Main',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                  ),
                ),
                Material(
                  color: Color.fromRGBO(20, 20, 20, 1),
                  child: InkWell(
                    onTap: () {
                      _displayDialog(context);
                    },
                    child: ListTile(
                      leading: Icon(
                        Icons.settings,
                        size: 30,
                        color: Colors.blue,
                      ),
                      title: Text(
                        'Settings',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ), // Populate the Drawer in the next step.
      ),
      appBar: AppBar(
        actions: <Widget>[_dateDropDown()],
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.menu,
              color: Colors.blue,
            ),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w300),
        ),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Color.fromRGBO(15, 15, 15, 1),
        child: RefreshIndicator(
          onRefresh: init,
          child: SingleChildScrollView(
            child: Wrap(children: <Widget>[
              if (data != null && data.length > 0)
                SizedBox(
                    height: 400,
                    width: 600,
                    child: SimpleLineChart.withData(data)),
              if (data != null && data.length > 0)
                SizedBox(
                  height: 400,
                  width: 600,
                  child:
                      SimpleBarChart.withData(getCumulativeMeasurements(data)),
                ),
              SizedBox(
                height: 400,
                width: 400,
                child: wrapToCard(
                    RaspImage(
                        "https://micromec.org:32004/snap/0?imageName=trialImage.jpg&colorSpace=RGB"),
                    "Camera 1"),
              ),
              SizedBox(
                height: 400,
                width: 400,
                child: wrapToCard(
                    RaspImage("http://10.84.112.21:8000/image.jpg"),
                    "Camera 2"),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
