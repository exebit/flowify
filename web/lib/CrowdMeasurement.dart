class CrowdMeasurement {
  final int time;
  final int peopleCount;
  final Camera camera;

  factory CrowdMeasurement.fromJson(Map m) {
    return CrowdMeasurement(
        time: m['ts'],
        peopleCount: m['peopleCount'],
        camera: Camera.fromJson(m['camera']));
  }

  CrowdMeasurement({this.time, this.peopleCount, this.camera});
}

class Camera {
  final int id;

  factory Camera.fromJson(Map data) {
    return Camera(
      int.parse(data['id']),
    );
  }

  Camera(this.id);
}
