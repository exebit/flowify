import 'package:flutter/material.dart';

class RaspImage extends StatefulWidget {
  final String url;
  RaspImage(this.url);

  @override
  _ImageState createState() => _ImageState();
}

class _ImageState extends State<RaspImage> {
  @override
  void initState() {
    super.initState();
    init();
  }

  bool loading = false;

  Future<void> init() async {
    try {
      await Future.delayed(Duration(seconds: 20));
      setState(() {
        loading = true;
      });
      print("refresh");
      await Future.delayed(Duration(milliseconds: 200));
      setState(() {
        loading = false;
      });
      init();
    } catch (e) {}
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        ),
        if (!loading)
          Padding(
            padding: const EdgeInsets.only(right: 8.0, top: 10.0),
            child: Image.network(
              widget.url,
              fit: BoxFit.contain,
            ),
          )
      ],
    );
  }
}
