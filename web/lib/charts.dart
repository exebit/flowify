import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:web/CrowdMeasurement.dart';
import 'package:web/api.dart';

charts.Color getRandColor(int index) {
  switch (index) {
    case 0:
      return charts.MaterialPalette.blue.shadeDefault;
    case 1:
      return charts.MaterialPalette.red.shadeDefault;
    case 2:
      return charts.MaterialPalette.green.shadeDefault;
    case 3:
      return charts.MaterialPalette.deepOrange.shadeDefault;
    default:
      return charts.MaterialPalette.yellow.shadeDefault;
  }
}

class SimpleLineChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final String title;
  SimpleLineChart(
    this.seriesList,
    this.title, {
    this.animate,
  });

  factory SimpleLineChart.withData(List<CrowdMeasurement> data) {
    return SimpleLineChart(
      //    _createData(data),
      _createData(data),
      "People per minute",
      animate: false,
    );
  }

  static List<charts.Series<LinearSales, int>> _createData(
      List<CrowdMeasurement> _data) {
    List<List<LinearSales>> data = getHourlyMeasurements(_data);

    return [
      charts.Series<LinearSales, int>(
        id: "0",
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data[0],
      ),
      if (data.length > 1)
        charts.Series<LinearSales, int>(
          id: "1",
          colorFn: (_, __) => charts.MaterialPalette.yellow.shadeDefault,
          domainFn: (LinearSales sales, _) => sales.year,
          measureFn: (LinearSales sales, _) => sales.sales,
          data: data[1],
        ),
      if (data.length > 2)
        charts.Series<LinearSales, int>(
          id: "2",
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (LinearSales sales, _) => sales.year,
          measureFn: (LinearSales sales, _) => sales.sales,
          data: data[2],
        )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return wrapToCard(
        charts.LineChart(seriesList,
            animate: animate,
            defaultRenderer: charts.LineRendererConfig(),
            domainAxis: charts.NumericAxisSpec(
                tickProviderSpec:
                    charts.BasicNumericTickProviderSpec(zeroBound: false))),
        this.title);
  }
}

Widget wrapToCard(Widget card, String title) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Card(
      elevation: 20,
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                      color: Colors.blue,
                      fontSize: 28.0,
                      fontWeight: FontWeight.w300),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 8.0, right: 0.0, top: 33.0, bottom: 2.0),
            child: card,
          ),
        ],
      ),
      color: Colors.grey[900],
    ),
  );
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}

class SimpleBarChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  SimpleBarChart(this.seriesList, {this.animate});

  factory SimpleBarChart.withData(List<CumulativeMeasurement> data) {
    return SimpleBarChart(_createData(data));
  }

  static List<charts.Series<CumulativeMeasurement, String>> _createData(
      List<CumulativeMeasurement> data) {
    return [
      charts.Series<CumulativeMeasurement, String>(
        id: 'People flow',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (CumulativeMeasurement sales, _) => sales.camera,
        measureFn: (CumulativeMeasurement sales, _) => sales.count,
        data: data,
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return wrapToCard(
        charts.BarChart(
          seriesList,
          animate: animate,
        ),
        "Cumulative per camera");
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
