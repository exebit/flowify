import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:web/CrowdMeasurement.dart';
import 'package:web/charts.dart';

List testData = [
  {
    "time": 1573856002,
    "peopleCount": 2,
    "camera": {"id": 0, "metadataorsomethingidk": "k"}
  },
  {
    "time": 1573856551,
    "peopleCount": 4,
    "camera": {"id": 0, "metadataorsomethingidk": "k"}
  },
  {
    "time": 1573856547,
    "peopleCount": 2,
    "camera": {"id": 0, "metadataorsomethingidk": "k"}
  },
  {
    "time": 1573856002,
    "peopleCount": 6,
    "camera": {"id": 1, "metadataorsomethingidk": "k"}
  },
  {
    "time": 1573856551,
    "peopleCount": 1,
    "camera": {"id": 1, "metadataorsomethingidk": "k"}
  },
  {
    "time": 1573856547,
    "peopleCount": 2,
    "camera": {"id": 2, "metadataorsomethingidk": "k"}
  },
  {
    "time": 1573856547 - 3600,
    "peopleCount": 4,
    "camera": {"id": 2, "metadataorsomethingidk": "k"}
  }
];

class CumulativeMeasurement {
  final String camera;
  final int count;

  CumulativeMeasurement(this.camera, this.count);
}

List<List<LinearSales>> getHourlyMeasurements(
    List<CrowdMeasurement> measurements) {
  List<List<LinearSales>> toReturn = List<List<LinearSales>>();
  Map<String, Map<int, int>> measurementsSorted = {};
  DateTime now = DateTime.now();
  measurements.forEach((CrowdMeasurement f) {
    if (measurementsSorted[f.camera.id.toString()] == null) {
      measurementsSorted[f.camera.id.toString()] = {};
    }
    DateTime t = DateTime.fromMillisecondsSinceEpoch(f.time * 1000);
    if (now.millisecondsSinceEpoch - t.millisecondsSinceEpoch > 3600 * 1000) {
      return;
    }
    if (measurementsSorted[f.camera.id.toString()][t.minute] == null) {
      measurementsSorted[f.camera.id.toString()][t.minute] = 0;
    }
    measurementsSorted[f.camera.id.toString()][t.minute] += f.peopleCount;
  });

  measurementsSorted.keys.forEach((_) {
    toReturn.add([]);
  });

  measurementsSorted.forEach((key, Map<int, int> val) {
    val.forEach((_key, _val) {
      toReturn[num.parse(key) - 1].add(LinearSales(_key, _val));
    });
  });
  return toReturn;
}

class HourlyMeasurement {
  final String camera;
  final int count;
  final String hour;

  HourlyMeasurement(this.camera, this.count, this.hour);
}

List<CumulativeMeasurement> getCumulativeMeasurements(
    List<CrowdMeasurement> measurements) {
  Map data = {};
  List<CumulativeMeasurement> f = List<CumulativeMeasurement>();

  measurements.forEach((CrowdMeasurement f) {
    if (data[f.camera.id] == null) {
      data[f.camera.id] = 0;
    }
    data[f.camera.id] += f.peopleCount;
  });

  data.forEach((key, val) {
    f.add(CumulativeMeasurement(key.toString(), val));
  });

  return f;
}

Future<List<CrowdMeasurement>> getMeasurements(String ip) async {
  Map body = {"query": "{measurements {peopleCount ts camera{id}}}"};

  Map m = await http
      .post(
    ip + "/graphql",
    body: body,
  )
      .then((http.Response response) {
    return json.decode(response.body);
  }).catchError((e) {
    print(e);
    return {
      "status": 404,
    };
  });
  List<CrowdMeasurement> newMap = [];
  print(m);
  m['data']['measurements'].forEach((val) {
    newMap.add(CrowdMeasurement.fromJson(val));
  });
  return newMap;
}
