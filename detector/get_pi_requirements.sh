#!/bin/bash
echo kala

# Get packages required for OpenCV

apt-get -y install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
apt-get -y install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
apt-get -y install libxvidcore-dev libx264-dev
apt-get -y install libatlas-base-dev

pip3 install opencv-python==3.4.6.27
# Get packages required for TensorFlow

# For now, downloading the TensorFlow builds from lhelontra's "TensorFlow on ARM" repository
# Thanks lhelontra for being super awesome!
# Will change to just 'pip3 install tensorflow' once newer versions of TF are added to piwheels

#pip3 install tensorflow

version=$(python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')

if [ $version == "3.7" ]; then
#wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v2.0.0/tflite_runtime-1.14.0-cp37-cp37m-linux_x86_64.whl
pip3 install tensorflow
#rm tensorflow-2.0.0-cp37-none-linux_armv7l.whl
fi

if [ $version == "3.5" ]; then
wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v1.14.0/tensorflow-1.14.0-cp35-none-linux_armv7l.whl
pip3 install tensorflow-1.14.0-cp35-none-linux_armv7l.whl
rm tensorflow-1.14.0-cp35-none-linux_armv7l.whl
fi

