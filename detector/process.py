from detect import detect_image
import time
import requests
import os


while True:
    count = 0   
    print("Starting")   
    try:
        count = detect_image()
        method = "POST"
        print(count)
        data={"query": "mutation { createMeasurement(cameraId:"+str(os.environ['CAMERA_ID'])+", peopleCount:"+str(count)+"){ measurement{ id } } }" }
        t = requests.post(os.environ['API_URL'], data=data)
        print(t.text)

    except Exception as e:
        print(e)
        pass
    time.sleep(10)